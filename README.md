# Ansible 101 - Intro and Setup :cool_doge:

This class will cover a quick intro to Ansible and where it fall in devops. Also introducing IAC in the light of Configuaration Management Tools and Orchestration Management Tools. 

## Intro to Infrastrure as Code (IAC)

Ansible is part of Infrastructure Code tool set. These are tools that help one declaritivly define your infrastrure in code and have it run and build. 

There are two categories of tool in IAC:
- Configuaration Management Tools
- Orchestration Management Tools

**Configuaration Management Tools** largely deal with "internal" configuration machines. So what packages are install, what are their verision, what files need to exits, configuration files, setups, starting of services and others. 

These include tools such as: Chef, Puppet and Ansible

**Orchestration Management Tools** Largely focus more on the "sping" up of machines from images or imutable objects, and setting these up in a network. These tool can also delcaritly define infrastructure in the cloud, such as VPCs and their internal workings. These tools are focuss on orchestrating how an entire system will operate.

These include tools as: Terraform, Cloudformation and Ansible


As you can see, ansible falls and bit in both. And these definitions have a lot of grey scales. They should help you frame tools and then it's for your to figure out the limits of these and how hard or easy it would be to do the same job in a different tool. 


## Ansible Intro and Setup

### Intro
Ansible work with system of a controler node and target or host nodes that get modified. 

Setup looks like:

**picture of worker node and target machine**


### Setup

1) Pre requisits
   1) Python 3.5 or higher
   2) Pip
2) Install controller node
   1) Package control (yum, brew, apt)
   2) pip (is good, however, does not creat default files)
   3) Binary
3) Setup another 2 machines with python
4) Setup ssh connection
   1) Password
   2) SSH connection (with password)
5) in /etc/ssh/sshd_config set PasswordAuthentication yes
6) sudo systemctl restart sshd.service
7) 

#### 3) setup ssh keys: 
* generate the ssh key with ssh-keygen

```
ssh-keygen -t ecdsa -b 521
```
* copy the public key on target servers

```
vim $HOME/.ssh/authorized_keys
```
or 

with ssh-copy-id

```
ssh-copy-id -i $HOME/.ssh/id_ecdsa <user>@<target_server>
```

* not to have to re-specify your key > use ssh agent

* check if you have an agent

```
ssh-add -l
```

Note : if you have an agent or a key > key is in the output

* to run an agent

```
eval `ssh-agent`
```


* another way to custom your ssh connection > ssh_config file

```
cat ~/.ssh/config
chmod 600 ~/.ssh/config

Host *
    User vagrant
    Port 22
    IdentityFile /myhome/.ssh/maclefprivee
    LogLevel INFO
```

### Ansible structure setup

#### Config File

Example: https://github.com/ansible/ansible/blob/devel/examples/ansible.cfg

#### Host File

Host files allows you to create groups of machines. 
You can define them under a namespace that groups these servers.

The following syntax is used: 

```
[name-space]
<ip.of.server.target> <option>
<ip.of.server.target> <option>
```

example of useful breakdown 
```
[webservers]
172.31.34.111 ansible_ssh_user=ec2-user
172.31.44.69 ansible_ssh_user=ec2-user

[db-server]
172.31.4.121 ansible_ssh_user=ec2-user

[load-balancer]
172.31.14.21 ansible_ssh_user=ec2-user

[bastion]
172.31.140.210 ansible_ssh_user=ec2-user

[test-webservers]
172.31.4.1 ansible_ssh_user=ec2-user
172.31.4.2 ansible_ssh_user=ec2-user

[test db-server]
172.31.0.1 ansible_ssh_user=ec2-user

```


### Ansible Adhoc-Commands 

These are command you run on the fly, that allow you to interact with your infrastructure. 

This is useful, perticualarlÃ§y when you want to query your infrastructure. These are usually single commands. 

The most common is used to just check connectivity.

```ansible all -m ping```

Syntax of Adhoc command

```ansible <what_group_of_server> -m <ansible_module>```

One very usefull Ansible module is the Shell. It allow you to run shell :) 

```ansible all -m shell -a "uptime"```

**check free space**


**Check environment variable**

**Variable list**
```BASH
  name: "{{ packages }}"
   state: present
  vars:
   packages:
   - httpd
   - httpd-tools
   - git
   - mysql
   - php
   - php-mbstring
   - php-intl
   - php-pdo
   - php-pdo_mysql
```
